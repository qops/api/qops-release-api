########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################


from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship, backref

from qops_desktop import db


class Base(db.Model):
    __abstract__ = True
    __bind_key__ = 'qops'

    id = db.Column(db.Integer, primary_key = True)
    created_on = db.Column(db.DateTime, default=db.func.now())
    updated_on = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())


class Release(Base):
    __tablename__ = 'release'

    identifier = db.Column(db.String(25))
    name = db.Column(db.String)
    version = db.Column(db.String)
    description = db.Column(db.String)
    theme_id = db.Column(db.Integer, ForeignKey('release_theme.id'))
    actual_work_start_date = db.Column(db.String)
    target_work_start_date = db.Column(db.String)
    actual_date_released = db.Column(db.String)
    target_release_date = db.Column(db.String)
    internal_code_name = db.Column(db.String)
    external_code_name = db.Column(db.String)
    kind_id = db.Column(db.Integer, ForeignKey('release_kind.id'))

    def get_next_identifier():
        return 'REL00001'


class ProductRelease(Base):
    __tablename__ = 'product_release'

    product_id = db.Column(db.Integer)
    release_id = db.Column(db.Integer)


class ProjectRelease(Base):
    __tablename__ = 'project_release'

    project_id = db.Column(db.Integer)
    release_id = db.Column(db.Integer)


class ReleaseKind(Base):
    __tablename__ = 'release_kind'

    name = db.Column(db.String)
    usage = db.Column(db.String)
    #releases = relationship('Release', backref='kind')


class ReleaseTheme(Base):
    __tablename__ = 'release_theme'

    name = db.Column(db.String)
    usage = db.Column(db.String)
    #releases = relationship('Release', backref='theme')
