########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################


# Import flask dependencies
from flask import Blueprint, request, render_template, \
    flash, g, session, redirect, url_for

import logging

# Import models
from .models import Release
from .models import ReleaseKind
from .models import ReleaseTheme


from .forms import ReleaseProfileForm

# Import the database object from the main app module
from qops_desktop import db

# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_release = Blueprint('release', __name__, url_prefix='/release')


@mod_release.context_processor
def store():
    store_dict = {'serviceName': 'Release',
                  'serviceDashboardUrl': url_for('release.dashboard'),
                  'serviceBrowseUrl': url_for('release.browse'),
                  'serviceNewUrl': url_for('release.new'),
                  }
    return store_dict

# Set the route and accepted methods


@mod_release.route('/', methods=['GET'])
def release():
    return render_template('release/release_dashboard.html')


@mod_release.route('/dashboard', methods=['GET'])
def dashboard():
    return render_template('release/release_dashboard.html')


@mod_release.route('/browse', methods=['GET'])
def browse():
    releases = Release.query.with_entities(Release.id, Release.identifier, Release.name, Release.version, Release.internal_code_name, Release.external_code_name, Release.actual_work_start_date, Release.target_work_start_date, Release.target_release_date,
                                           Release.actual_date_released, ReleaseKind.name.label('kind_id'), ReleaseTheme.name.label('theme_id')).join(ReleaseKind, Release.kind_id == ReleaseKind.id).join(ReleaseTheme, Release.theme_id == ReleaseTheme.id).all()

    return render_template('release/release_browse.html', releases=releases)


@mod_release.route('/new', methods=['GET', 'POST'])
def new():
    release = Release()
    form = ReleaseProfileForm(request.form)
    form.kind_id.choices = [(k.id, k.name) for k in ReleaseKind.query.all()]
    form.theme_id.choices = [(t.id, t.name) for t in ReleaseTheme.query.all()]
    if request.method == 'POST':
        logger = logging.getLogger('qops')
        logger.debug('IN RELEASE.NEW(POST)')
        logger.debug('Release Identifier        : {0}'.format(
            form.identifier.data))
        logger.debug('Release Name              : {0}'.format(form.name.data))
        logger.debug('Release Description       : {0}'.format(
            form.description.data))
        logger.debug(
            'Release Kind              : {0}'.format(form.kind_id.data))
        logger.debug('Release Theme             : {0}'.format(
            form.theme_id.data))
        logger.debug('Release Internal Code Name: {0}'.format(
            form.internal_code_name.data))
        logger.debug('Release External Code Name: {0}'.format(
            form.external_code_name.data))
        form.populate_obj(release)
        release.identifier = Release.get_next_identifier()
        db.session.add(release)
        db.session.commit()
        return redirect(url_for('release.browse'))
    return render_template('release/release_new.html', release=release, form=form)


@mod_release.route('/profile', methods=['GET', 'POST'])
@mod_release.route('/profile/<int:release_id>/profile', methods=['GET', 'POST'])
def profile(release_id=None):
    release = Release.query.get(release_id)
    form = ReleaseProfileForm(obj=release)
    form.kind_id.choices = [(k.id, k.name) for k in ReleaseKind.query.all()]
    form.theme_id.choices = [(t.id, t.name) for t in ReleaseTheme.query.all()]
    if request.method == 'POST':
        form = ReleaseProfileForm(request.form)
        form.populate_obj(release)
        db.session.add(release)
        db.session.commit()
        return redirect(url_for('release.browse'))
    return render_template('release/release_profile.html', release=release, form=form)


@mod_release.route('/view', methods=['GET', 'POST'])
@mod_release.route('/view/<int:release_id>/view', methods=['GET', 'POST'])
def release_view(release_id=None):
    #release = Release.query.get(release_id)
    form = ReleaseProfileForm(obj=release)
    form.kind_id.choices = [(k.id, k.name) for k in ReleaseKind.query.all()]
    form.theme_id.choices = [(t.id, t.name) for t in ReleaseTheme.query.all()]
    if request.method == 'POST':
        form = ReleaseProfileForm(request.form)
        form.populate_obj(release)
        db.session.add(release)
        db.session.commit()
        return redirect(url_for('release.browse'))
    return render_template('release/release_view.html', release=release,
                           form=form)


@mod_release.route('/profile/dashboard', methods=['GET'])
@mod_release.route('/profile/<int:release_id>/dashboard', methods=['GET'])
def release_dashboard(release_id=None):
    if release_id:
        release = Release.query.get(release_id)
    else:
        release = None
    return render_template('release/release_single_dashboard.html', release=release)


@mod_release.route('/profile/user_stories', methods=['GET'])
@mod_release.route('/profile/<int:release_id>/user_stories', methods=['GET'])
def release_user_stories(release_id=None):
    if release_id:
        release = Release.query.get(release_id)
    else:
        release = None
    return render_template('release/release_single_user_stories.html', release=release)


@mod_release.route('/profile/communication', methods=['GET', 'POST'])
@mod_release.route('/profile/<int:release_id>/communication', methods=['GET'])
def release_communication(release_id=None):
    if release_id:
        release = Release.query.get(release_id)
    else:
        release = None
    return render_template('release/release_single_communication.html', release=release)


@mod_release.route('/profile/documents', methods=['GET', 'POST'])
@mod_release.route('/profile/<int:release_id>/documents', methods=['GET'])
def release_documents(release_id=None):
    if release_id:
        release = Release.query.get(release_id)
    else:
        release = None
    return render_template('release/release_single_documents.html', release=release)


@mod_release.route('/profile/builds', methods=['GET', 'POST'])
@mod_release.route('/profile/<int:release_id>/builds', methods=['GET'])
def release_builds(release_id=None):
    if release_id:
        release = Release.query.get(release_id)
    else:
        release = None
    return render_template('release/release_single_builds.html', release=release)


@mod_release.route('/profile/requirements', methods=['GET', 'POST'])
@mod_release.route('/profile/<int:release_id>/requirements', methods=['GET'])
def release_requirements(release_id=None):
    if release_id:
        release = Release.query.get(release_id)
    else:
        release = None
    return render_template('release/release_single_requirements.html', release=release)


@mod_release.route('/profile/customers', methods=['GET', 'POST'])
@mod_release.route('/profile/<int:release_id>/customers', methods=['GET'])
def release_customers(release_id=None):
    if release_id:
        release = Release.query.get(release_id)
    else:
        release = None
    return render_template('release/release_single_customers.html', release=release)


@mod_release.route('/profile/locations', methods=['GET', 'POST'])
@mod_release.route('/profile/<int:release_id>/builds', methods=['GET'])
def release_locations(release_id=None):
    if release_id:
        release = Release.query.get(release_id)
    else:
        release = None
    return render_template('release/release_single_locations.html', release=release)


@mod_release.route('/profile/issues', methods=['GET', 'POST'])
@mod_release.route('/profile/<int:release_id>/issues', methods=['GET'])
def release_issues(release_id=None):
    if release_id:
        release = Release.query.get(release_id)
    else:
        release = None
    return render_template('release/release_single_issues.html', release=release)


@mod_release.route('/profile/test-cases', methods=['GET', 'POST'])
@mod_release.route('/profile/<int:release_id>/test-cases', methods=['GET'])
def release_test_cases(release_id=None):
    if release_id:
        release = Release.query.get(release_id)
    else:
        release = None
    return render_template('release/release_single_test_cases.html', release=release)


@mod_release.route('/profile/tasks', methods=['GET'])
@mod_release.route('/profile/<int:release_id>/tasks', methods=['GET'])
def release_tasks(user_story_id=None):
    if release_id:
        release = Release.query.get(release_id)
    else:
        release = None
    return render_template('release/release_single_tasks.html', release=release)
